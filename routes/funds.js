var express = require('express');
var router = express.Router();
var request = require('request');

router.post('/transfer', (req, res) => {
    console.log("Received request for fund transfer.. ");
    console.log("Serving from version 3");
    request.post(process.env.FUND_TRANSFER_URL, { json: req.body }, (err, response, body) => {
        if (err) {
            console.log("Fund transfer request failed.. Returning error..");
            console.log(body);
            res.status(500);
            res.json(body);
        } else {
            console.log("Fund transfer request successful.. Returning response..");
            console.log(body);
            res.json(body);
        }
    })
})

module.exports = router;
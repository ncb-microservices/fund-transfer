var express = require('express');
var router = express.Router();

var desiredLoadFactor;
var shouldRun=false;

function blockCpuFor(ms) {
    console.log("Blocking CPU for " + ms)
	var now = new Date().getTime();
	var result = 0
	while(shouldRun) {
        result = 1;
        randomNumber = Math.random();
        for(i=1;i<=randomNumber;i++){
            result = result * i;
        }
		if (new Date().getTime() > now +ms)
			return;
	}	
}


function start() {
	shouldRun = true;
	blockCpuFor(1000 * desiredLoadFactor);
}

router.post('/loadcpu', (req, res, next)=>{
    console.log('Initiating load....');
    console.log("Loading CPU for " + req.query.time + " seconds")
    desiredLoadFactor = req.query.time;
    start();
    shouldRun = false;
    res.json({});
})
module.exports = router;